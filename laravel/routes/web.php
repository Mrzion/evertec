<?php

use App\Http\Controllers\Order\OrderController;
use App\Http\Controllers\Transaction\TransactionController;
use App\Services\WebCheckOut\{PaymentService,AuthService};
use Carbon\Carbon;
use Illuminate\Http\Client\Request as ClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/login');
Auth::routes();
/*---------------------------------------------------
                 Public Route
------------------------------------------------------- */
route::group([ 'middleware' => ['auth','verified']],function(){
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::get('transactions',[TransactionController::class,'index'])->name('transaction.index');
    Route::get('transactions/{transaction}/resume',[TransactionController::class,'show'])->name('transaction.resume');
    Route::resource('order',OrderController::class);
    Route::get('order/resume/{order}',[OrderController::class,'resume'])->name('order.resume');

});




