@extends('layouts.app')
@section('content')
<!-- Breadcrumb -->
<!-- begin page-header -->
<!------------------- Content Wrapper Start ----------------------->
<div class="container-fluid mb-5" >
    <div class="row justify-content-center">
        <div class="col-md-7 col-12">
            <div class="card-wrapper">
                <!-- Custom form validation -->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header ">
                        <div class="d-flex justify-content-between">
                            <h3 class="mb-0">Lists Transactions</h3>
                        </div>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                        @include('transactions.table')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
