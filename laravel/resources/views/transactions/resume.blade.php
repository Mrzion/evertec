@extends('layouts.app')
@section('content')

<!------------------- Content Wrapper Start ----------------------->
<div class="container-fluid mb-5" >
    <div class="row justify-content-center">
        <div class="col-md-8 col-12">
            <div class="card-wrapper">
                <!-- Custom form validation -->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header ">
                        <h3 class="mb-0">Resumen</h3>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                         <div class="container">
                             <div class="row">
                                <div class="col-md-4 col-12">
                                    <label for="" class="form-control-label">{{__('RequestId')}}</label>
                                    <label for="" class="form-control">{{$transaction->requestId}}</label>
                                </div>
                                <div class="col-md-4 col-12">
                                    <label for="">{{__('Status')}}</label>

                                    <label for="" class="form-control">{{$transaction->status->status}}</label>
                                </div>
                                <div class="col-md-4 col-12">
                                    <label for="">{{__('Name')}}</label>

                                    <label for="" class="form-control">{{$transaction->request->payer->name}}</label>
                                </div>
                                <div class="col-md-4 col-12">
                                    <label for="" class="form-control-label">{{__('Email')}}</label>

                                    <label for="" class="form-control">{{$transaction->request->payer->email}}</label>
                                </div>
                                <div class="col-md-4 col-12">
                                    <label for="" class="form-control-label">{{__('Mobile')}}</label>
                                    <label for="" class="form-control">{{$transaction->request->payer->mobile}}</label>
                                </div>
                             </div>
                             <div class="row">
                                <div class="col-md-4 col-12">
                                    <label for="" class="form-control-label">{{__('Payment')}}</label>
                                    <label for="" class="form-control">{{$transaction->request->payment->reference}}</label>
                                </div>
                                <div class="col-md-4 col-12">
                                    <label for="" class="form-control-label">{{__('Description')}}</label>
                                    <label for="" class="form-control">{{$transaction->request->payment->description}}</label>
                                </div>
                                <div class="col-md-4 col-12">
                                    <label for="" class="form-control-label">{{__('Total')}}</label>
                                    <label for="" class="form-control">{{$transaction->request->payment->amount->total }} {{$transaction->request->payment->amount->currency }}</label>
                                </div>
                             </div>
                         </div>



                        <div class="d-flex justify-content-end">
                            <a href="{{route('transaction.index')}}" class="btn btn-danger">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
