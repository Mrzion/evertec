<table class="table table-responsive">
    <thead>
        <th>{{__('Option')}}</th>
        <th>{{__('Id Transaction')}}</th>
        <th>{{__('Status')}}</th>
        <th>{{__('Message')}}</th>
        <th>{{__('Nro Order')}}</th>

        <th>{{__('Date')}}</th>

    </thead>
    <tbody>
        @foreach ($transactions as $transaction )
        <tr>
            <td>
                <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                    <a type="button" class="btn btn-outline-success" href="{{route('transaction.resume',$transaction->id)}}">Resume</a>
                  </div>
            </td>
            <td>{{$transaction->requestId}}</td>
            <td class="">{{$transaction->status}}</td>
            <td>{{$transaction->message}}</td>
            <td>{{$transaction->order_id}}</td>
            <td>{{$transaction->date}}</td>
        </tr>
        @endforeach

    </tbody>
</table>
<div class="d-flex justify-content-center">
    {{$transactions->links()}}
</div>
