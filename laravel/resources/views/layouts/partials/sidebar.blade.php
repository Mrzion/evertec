<ul class="nav">
    <li class="nav-item">
      <a class="nav-link active" href="{{route('order.create')}}">New order</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('order.index')}}">List Order</a>
    </li>
    @if(Auth::user()->is_admin)
    <li class="nav-item">
      <a class="nav-link " href="{{route('transaction.index')}}">Transactions</a>
    </li>
    @endif
  </ul>
