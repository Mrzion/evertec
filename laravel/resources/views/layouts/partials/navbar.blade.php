<nav class="navbar navbar-top navbar-expand border-bottom bg-white">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
           <img src="{{asset('img/logo.jpg')}}" width="250" height="75" alt="evertec">
            {{------------------- Navbar Nav Start -----------------------}}
            <ul class="navbar-nav align-items-center ml-md-auto">
                <li class="nav-item d-xl-none">

                    {{------------------- Sidenav toggler -----------------------}}
                    <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                        </div>
                    </div>
                </li>
                <li class="nav-item d-sm-none">
                    <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                        <i class="ni ni-zoom-split-in"></i>
                    </a>
                </li>


            </ul>
            {{--------------***** Navbar Nav End *****------------------}}

            {{------------------- Navbar Nav Start -----------------------}}
            <ul class="navbar-nav align-items-center ml-auto ml-md-0">
                <li class="nav-item dropdown">

                    {{------------------------------------------}}
                    <a class="nav-link pr-0" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                        <div class="media align-items-center">

                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold text-dark">{{ Auth::user()->email }} ^</span>
                            </div>

                        </div>

                    </a>

                    {{------------------------------------------}}
                    <div class="dropdown-menu dropdown-menu-right">

                        {{------------------------------------------}}
                        <div class="dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Bienvenido!</h6>
                        </div>

                        {{------------------------------------------}}


                        {{------------------------------------------}}
                        <div class="dropdown-divider"></div>

                        {{------------------------------------------}}
                        <a href="javascript:void(0);" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out text-red">
                                <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
                                <polyline points="16 17 21 12 16 7"></polyline>
                                <line x1="21" y1="12" x2="9" y2="12"></line>
                            </svg>
                            <span>Salir</span>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </a>

                    </div>
                </li>
            </ul>
            {{--------------***** Navbar Nav End *****------------------}}

        </div>
    </div>
</nav>
