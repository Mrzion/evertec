<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" xml:lang="{{ app()->getLocale() }}" xmlns="http://www.w3.org/1999/xhtml">
{{--<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">--}}

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @guest
    @else
        <meta name="user" content="{{ Auth::user()->id }}">
    @endguest
<!--OpenGraph metadata-->
    @include('layouts.partials.css')
    @stack('css')
    @stack('scripts-up')
    {{--FAVICON--}}

    {{--LDJSON--}}
    {{-- @include('layouts.partials.ldjson')
 --}}
    {{--Google analitycs--}}
{{--     @include('layouts.partials.google')
 --}}
    <title>{{ config('app.name') }}</title>
</head>

<body class="p-0">
<main class="">
    @auth
        @include('layouts.partials.navbar')
    <header>
        @include('layouts.partials.sidebar')

        @stack('header-plus')
    </header>
    @endauth

<div id="content" class="content py-3" >
    @yield('content')
</div>
{{-- footer --}}
    {{-- @include('admin.layouts.partials.footer') --}}

</main>
@include('layouts.partials.js')
@stack('scripts-down')
</body>

</html>
