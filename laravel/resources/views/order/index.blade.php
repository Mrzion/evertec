@extends('layouts.app')
@section('content')
<!-- Breadcrumb -->
{{-- @include('admin.layouts.partials.breadcrumb', ['inicio' => 'Inicio','path' => 'Crear']) --}}
<!-- begin page-header -->
<!------------------- Content Wrapper Start ----------------------->
<div class="container-fluid mb-5" >
    <div class="row justify-content-center">
        <div class="col-md-8 col-12">
            <div class="card-wrapper">
                <!-- Custom form validation -->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header ">
                        <div class="d-flex justify-content-between">
                            <h3 class="mb-0">Lists Order</h3>
                            <a class="btn btn-sm btn-primary" href="{{route('order.create')}}">New Order</a>
                        </div>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                        @include('order.table')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
