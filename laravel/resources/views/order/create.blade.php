@extends('layouts.app')
@section('content')
<!-- Breadcrumb -->
{{-- @include('admin.layouts.partials.breadcrumb', ['inicio' => 'Inicio','path' => 'Crear']) --}}
<!-- begin page-header -->
<!------------------- Content Wrapper Start ----------------------->
<div class="container-fluid mb-5" >
    <div class="row justify-content-center">
        <div class="col-md-6 col-12">
            <div class="card-wrapper">
                <!-- Custom form validation -->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header ">
                        <h3 class="mb-0">Procesos De Checkout</h3>
                    </div>

                    <!-- Card body -->
                    <div class="card-body">
                        @include('layouts.partials.errors')
                        <form action="{{route('order.store')}}" method="POST">
                            @csrf
                            @include('order.fields')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
