<table class="table table-responsive">
    <thead>
        <th width="5%">{{__('Option')}}</th>
        <th width="10%">{{__('Name')}}</th>
        <th width="10%">{{__('Email')}}</th>
        <th width="5%">{{__('Mobile')}}</th>
        <th width="5%">{{__('Status')}}</th>
        <th width="10%">{{__('Created at')}}</th>

    </thead>
    <tbody>
        @foreach ($orders as $order )
        <tr>
            <td>
                <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                    <a type="button" class="btn btn-outline-success" href="{{route('order.resume',$order->id)}}">Resume</a>
                  </div>
            </td>
            <td>{{$order->customer_name}}</td>
            <td>{{$order->customer_email}}</td>
            <td>{{$order->customer_mobile}}</td>
            <td class=" font-weight-bold {{($order->status == "REJECTED")?'text-danger':''}}">{{$order->status}}</td>
            <td>{{Carbon\Carbon::parse($order->created_at)->format('d/m/Y H:m')}}</td>
        </tr>
        @endforeach

    </tbody>
</table>
<div class="d-flex justify-content-center">
    {{$orders->links()}}
</div>
