<div class="container mb-5">
    <div class="row justify-content-center">
        <div class="text-center">
            <h4>Products</h4>
            <div class="card">
                <img class="card-img-top" src="{{asset('img/shoes.jpg')}}" alt="Card image cap">
                <div class="card-body">
                    <p>Adidas Bravada Shoes - Black | adidas US</p>
                    <div class="d-flex justify-content-end">
                        <p class="text-right font-weight-bold">Price: <span>{{env('PRICE_ARTICLE')}}</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <h4>Personal information</h4>
        </div>
        <div class="col-md-6 col-12 p-2">
            <label for="" class="fomr-control-label">{{__('Name')}}</label>
            <input type="text" class="form-control" id="customer_name" value="{{old('customer_name')}}" placeholder="name" minlength="3" maxlength="70"  name="customer_name"  required>
        </div>
        <div class="col-md-6 col-12 p-2">
            <label for="" class="fomr-control-label">{{__('Email')}}</label>
            <input type="email" class="form-control" value="{{old('customer_email')}}" placeholder="example@example.com"  minlength="5" maxlength="70" name="customer_email" required>
        </div>
        <div class="col-md-6 col-12 p-2">
            <label for="" class="fomr-control-label">{{__('Phone')}}</label>
            <input type="text" class="form-control" minlength="7" maxlength="30" placeholder="(999) 9999-9999" id="customer_mobile" value="{{old('customer_mobile')}}" name="customer_mobile" required>
        </div>
        <div class="col-md-6 col-12 p-2">
            <label for="" class="fomr-control-label">{{__('Payment Method')}}</label>
            <select name="type" class="form-control" id="" required>
                <option value="1">PlaceToPay</option>
            </select>
        </div>
        <div class="col-md-6 col-12 p-2">
            <label for="" class="fomr-control-label">{{__('Price')}}</label>
            <input type="text" class="form-control text-right" value="{{env('PRICE_ARTICLE')}}" name="price" required readonly>
        </div>
    </div>


    <div class="d-flex justify-content-end">
        <button class="btn btn-success" type="submit">Procesar</button>
    </div>
</div>
@push('scripts-down')
<script type="text/javascript" src="{{ asset('js/inputmask/jquery.inputmask.js')}}"></script>
<script>
    $("#customer_mobile").inputmask({"mask": "(999) 999-9999",'removeMaskOnSubmit': true});
</script>
@endpush
