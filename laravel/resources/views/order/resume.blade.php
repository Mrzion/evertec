@extends('layouts.app')
@section('content')

<!------------------- Content Wrapper Start ----------------------->
<div class="container-fluid mb-5" >
    <div class="row justify-content-center">
        <div class="col-md-6 col-12">
            <div class="card-wrapper">
                <!-- Custom form validation -->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header ">
                        <h3 class="mb-0">Resumen</h3>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">

                        <form action="{{route('order.store')}}" method="POST">
                            @csrf
                            <div class="container mb-5">
                                <div class="row justify-content-center">
                                    <div class="text-center">
                                        <h4>Product: <span class="text-primary">Adidas Bravada</span></h4>
                                        <div class="card">
                                            <img class="card-img-top" src="{{asset('img/shoes.jpg')}}" alt="Card image cap">
                                            <div class="card-body">
                                                <p>Adidas Bravada Shoes - Black | adidas US</p>
                                                <div class="d-flex justify-content-end">
                                                    <p class="text-right font-weight-bold">Price: <span>{{env('PRICE_ARTICLE')}}</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row p-3">
                                    <div class="col-12">
                                        <h4>Personal information</h4>
                                    </div>
                                    <div class="col-md-6 col-12 p-2">
                                        <label for="" class="form-control-label">{{__('Name')}}</label>
                                        <label for="" class="form-control">{{$order->customer_name}}</label>
                                    </div>
                                    <div class="col-md-6 col-12 p-2">
                                        <label for="" class="form-control-label">{{__('Email')}}</label>
                                        <label for="" class="form-control">{{$order->customer_email}}</label>
                                    </div>
                                    <div class="col-md-6 col-12 p-2">
                                        <label for="" class="form-control-label">{{__('Phone')}}</label>
                                        <label for="" class="form-control">{{$order->customer_mobile}}</label>
                                    </div>
                                    <div class="col-md-6 col-12 p-2">
                                        <label for="" class="form-control-label">{{__('Status Pay')}}</label>
                                        <label for="" class="form-control">{{$order->status}}</label>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <a class="btn btn-danger mr-2" href="{{route('order.index')}}">Back</a>
                                        @if($order->status=="REJECTED")
                                            <a class="btn btn-success" href={{route('order.create')}}>Retry</a>
                                        @elseif($order->status=="CREATED" || $order->status=='PENDING')
                                            <a class="btn btn-success" href={{$order->transaction->processUrl}}>Process Payment</a>
                                        @else
                                        <span class="font-weight-bold text-sucess text-green btn">Order Paid</span>

                                    @endif

                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
