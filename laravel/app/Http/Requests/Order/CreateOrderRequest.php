<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;
/**
 * Request costruido para validar la data de crear la orden
 */
class CreateOrderRequest extends FormRequest
{
    protected function prepareForValidation()
    {
        $this->merge([
            'amount'=>env('10000'),
            'status'=>'CREATED'
        ]);
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_name'=>'required|max:40|min:3',
            'customer_email'=>'required|max:70|min:5',
            'customer_mobile'=>'required|max:50|min:7',
        ];
    }
}
