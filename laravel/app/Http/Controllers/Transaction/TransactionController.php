<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Responsable\Transaction\TransactionIndexResponsable;
use App\Responsable\Transaction\TransactionShowResponsable;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index(TransactionIndexResponsable $transactionIndexResponsable)
    {
        return $transactionIndexResponsable;
    }

    public function show(Transaction $transaction)
    {


        return new TransactionShowResponsable($transaction);
    }


}
