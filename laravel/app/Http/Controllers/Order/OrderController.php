<?php

namespace App\Http\Controllers\Order;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Repositories\Order\OrderRepository;
use App\Http\Requests\Order\CreateOrderRequest;
use App\Repositories\Transaction\TransactionRepository;
use App\Responsable\Order\{OrderCreateResponsable,OrderIndexResponsable,OrderResumeResponsable};
use App\Services\PaymentMethod\GlobalService;
use App\Services\Transaction\GenerateTransactionService;
use App\Services\WebCheckOut\PaymentService;
use Exception;

class OrderController extends Controller
{
    private $repository;
    private $transactionRepository;
    public function __construct(OrderRepository $repository,TransactionRepository $transactionRepository)
    {
        $this->repository = $repository;
        $this->transactionRepository = $transactionRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(OrderIndexResponsable $orderIndexResponsable)
    {
        return $orderIndexResponsable;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(OrderCreateResponsable $orderCreateResponsable)
    {
        return $orderCreateResponsable;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * creo inicialmente la order y le asocio la respuesta de placeToPay y lo guardo como una transaccion
     */
    public function store(CreateOrderRequest $request)
    {
        try{
            DB::beginTransaction();
            $order =  $this->repository->create($request->all());
            // Use Strategy
                $response=GlobalService::take($request,$order,(object)[
                    'transactionRepository'=>$this->transactionRepository,
                    'orderRepository'=>$this->repository
                ])->response;
               DB::commit();
                if(isset($response->transaction)){
                    return new OrderResumeResponsable($order);
                }
            throw new Exception("Error Processing Request", 1);
        } catch (\Throwable $e) {
            DB::rollback();
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     *Creation of the purchase summary
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function resume(Order $order)
    {
       return new OrderResumeResponsable($order);
    }
}
