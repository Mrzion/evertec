<?php

namespace App\Strategy\App;

use App\Contracts\StrategyInterface;

class Context
{
    private $strategy;

    public function __construct(StrategyInterface $strategy)
    {
        $this->strategy = $strategy;
    }

    public function executeStrategy($system)
    {
        return $this->strategy->run($system);
    }
}
