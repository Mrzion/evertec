<?php

namespace App\Strategy\PaymentMethod;

use App\Contracts\StrategyInterface;
use App\Services\WebCheckOut\PaymentService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class PlaceToPayStrategy implements StrategyInterface
{
    protected $model;

    protected $request;


    public function __construct(Model $model,Request $request)
    {
        $this->model=$model;
        $this->request=$request;
    }

    public function run(Model $model){
        //execute service
       return json_decode(PaymentService::take($this->request,$this->model)->response);
    }
}
