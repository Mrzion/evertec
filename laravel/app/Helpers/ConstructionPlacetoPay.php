<?php
/**
 *
 * Este metodo es para crear los parametros iniciales para auth CheckOut
 * @author Mario Rangel
 */
    function parameters(){
        $seed = date('c');
        $seed_expiration =date('c',mktime(date("G"),date('i')+env('TIME_EXPIRATION'),date("s")));
        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
        } else {
            $nonce = mt_rand();
        }

        $nonceBase64 = base64_encode($nonce);
        return (Object)[
            'seed'=>$seed,
            'seed_expiration'=>$seed_expiration,
            'nonce'=>$nonce,
            'nonceBase64'=> $nonceBase64,
            'tranKey'=>base64_encode(sha1($nonce . $seed . env('TRANKEY'), true))
        ];
    }
