<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @param Request $request de la peticion
 * Este metodo es para crear la data para el envio del pago|
 * @author Mario Rangel
 */
function payload(Request $request,$search=false)
    {
        $parameters= parameters();
        $data = (object)
                    [
                    'login'=>env('LOGIN'),
                    'seed'=>$parameters->seed,
                    'nonce'=>$parameters->nonceBase64,
                    'tranKey'=>$parameters->tranKey
                ];
            if(!$search){
                $payer =(object)[
                    "document"=> "8467451900",
                    "documentType"=>"CC",
                    "surname"=>Auth::user()->name,
                   "name"=> $request->customer_name,
                   "email"=> $request->customer_email,
                   "mobile"=> $request->customer_mobile,
                ];
                $buyer=(object)[
                    "document"=> "8467451900",
                    "documentType"=> "CC",
                    "name"=> "Mario Rangel",
                    "surname"=> 'mrzion',
                    "email"=> "mrzion94@gmail.com",
                    "mobile"=> '584128384105'
                ];

                $datapayment= (object)[
                    "reference"=> "5976030f5575d",
                    "description"=> "Pago básico de prueba",
                    "amount"=>(object)[
                        "currency"=> "COP",
                        "total"=> env('PRICE_ARTICLE')
                    ]
                ];
                return (object)[
                    "auth" => $data,
                    'locale'=>"es_CO",
                    "payer"=>$payer,
                    "buyer"=>$buyer,
                    "payment"=>$datapayment,
                    "expiration"=> $parameters->seed_expiration,
                    "returnUrl"=> route('order.resume',$request->id),
                    "ipAddress"=> "127.0.0.1",
                    "userAgent"=>"PlacetoPay"

                ];
            }
            return (object)[
                "auth" => $data,
            ];

    }
