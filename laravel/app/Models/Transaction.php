<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * MODEL DE TRANSACCION
 */
class Transaction extends Model
{
    use HasFactory;

    protected $fillable =[
        'status',
        'reason',
        'message',
        'date',
        'requestId',
        'processUrl',
        'order_id',
    ];

    public function order():BelongsTo{
     return $this->belongsTo(\App\Models\Order::class);
    }
}
