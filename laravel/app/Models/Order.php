<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
/**
 * MODEL DE ORDER
 */
class Order extends Model
{
    use HasFactory;

    protected $fillable=[
        'customer_name',
        'customer_email',
        'customer_mobile',
        'status',
    ];

    protected $casts=[
        'status'=>'string'
    ];

    public function transaction():HasOne
    {
        return $this->hasOne(\App\Models\Transaction::class);
    }
}
