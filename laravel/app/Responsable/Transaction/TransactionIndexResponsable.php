<?php

namespace App\Responsable\Transaction;

use App\Repositories\Transaction\TransactionRepository;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Auth;

class TransactionIndexResponsable implements Responsable
{
    private $repository;

    public function __construct(TransactionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function toResponse($request)
    {
        return view('transactions.index',['transactions'=>$this->repository->getTransactionPaginate()]);
    }
}
