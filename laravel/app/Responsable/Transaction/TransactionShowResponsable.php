<?php

namespace App\Responsable\Transaction;

use App\Models\Transaction;
use App\Services\WebCheckOut\GetTransactionService;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Auth;

class TransactionShowResponsable implements Responsable
{
    private $transaction;

    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    public function toResponse($request)
    {
        $request->merge([
            'requestId'=>$this->transaction->requestId
        ]);
        // return json_decode(GetTransactionService::take($request,$this->transaction->order)->response);
        return view('transactions.resume',['transaction'=>json_decode(GetTransactionService::take($request,$this->transaction->order)->response)]);
    }
}
