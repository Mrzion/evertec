<?php

namespace App\Responsable\Order;

use App\Repositories\Order\OrderRepository;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Auth;


/**
 * Responsable de Ordenes
 */
class OrderIndexResponsable implements Responsable
{
    private $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function toResponse($request)
    {
        return view('order.index',['orders'=>$this->repository->getOrdersPaginate()]);
    }
}
