<?php

namespace App\Responsable\Order;

use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Auth;


/**
 * Responsable dde Ordenes
 */
class OrderCreateResponsable implements Responsable
{

    public function toResponse($request)
    {
        return view('order.create');
    }
}
