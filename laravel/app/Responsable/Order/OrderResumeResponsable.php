<?php

namespace App\Responsable\Order;

use App\Models\Order;
use App\Services\WebCheckOut\GetTransactionService;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Auth;

/**
 * Responsable de Ordenes Resumen
 */
class OrderResumeResponsable implements Responsable
{
    private $order;
    private $url;

    public function __construct(Order $order)
    {
        $this->order = $order->load('transaction');
    }

    public function toResponse($request)
    {
        return view('order.resume',['order'=>$this->order,'transaction'=>$this->affectOrder($request)]);
    }

    public function affectOrder($request){
        $transaction=json_decode(GetTransactionService::take($request,$this->order)->response);
        $this->order->update(['status'=>$transaction->status->status]);
        return $transaction;
    }
}
