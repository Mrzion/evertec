<?php

namespace App\Repositories;

use App\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class Repository implements RepositoryInterface
{
    /**
     * @var Illuminate\Database\Eloquent\Model $model model.
     *
     */
    protected $model;

    /**
     * @var array $relations relations.
     *
     */
    private $relations;

    /**
     * Repository constructor.
     *
     */
    public function __construct(Model $model, array $relations = [])
    {
        // Set Data
        $this->model     = $model;
        $this->relations = $relations;
    }

    /**
     * Get All.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        return (!empty($this->relations))
        ? $this->model::with($this->relations)->get()
        : $this->model::get();
    }

    /**
     * Get Index.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return (!empty($this->relations))
        ? $this->model::with($this->relations)->get()
        : $this->model::get();
    }

    /**
     * Get.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(int $id)
    {
        return (!empty($this->relations))
        ? $this->model::with($this->relations)->whereId($id)->get()
        : $this->model::get();
    }

    /**
     * Find.
     *
     * @param  mixed  $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find($data)
    {
        return $this->model::with($this->relations)->find($data);
    }

    /**
     * First Or New.
     *
     * @param  mixed  $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function firstOrNew($data)
    {
        return $this->model::firstOrNew($data);
    }

    /**
     * Where.
     *
     * @param  mixed  $atribute
     * @param  mixed  $data
     * @return \Illuminate\Http\Response
     */
    public function where($atribute, $data)
    {
        return $this->model::with($this->relations)->where($atribute, $data);
    }

    /**
     * Where In.
     *
     * @param  mixed  $atribute
     * @param  mixed  $data
     * @return \Illuminate\Http\Response
     */
    public function whereIn($atribute, $data)
    {
        return $this->model::with($this->relations)->whereIn($atribute, $data);
    }

    /**
     * Where Not In.
     *
     * @param  mixed  $atribute
     * @param  mixed  $data
     * @return \Illuminate\Http\Response
     */
    public function whereNotIn($atribute, $data)
    {
        return $this->model::with($this->relations)->whereNotIn($atribute, $data);
    }

    /**
     * Where Raw.
     *
     * @param  mixed  $query
     * @param  mixed  $variables
     * @return \Illuminate\Http\Response
     */
    public function whereRaw($query, $variables)
    {
        return $this->model::with($this->relations)->whereRaw($query, $variables);
    }

    /**
     * Create.
     *
     * @param  mixed  $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($data)
    {
        return $this->model::create($data);
    }

    /**
     * Update.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update(Model $model)
    {
        return $model->save();
    }

    /**
     * Save.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function save(Model $model)
    {
        return $model->saveOrFail();
    }

    /**
     * Delete.
     *
     * @param  mixed  $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function delete($data)
    {
        return $this->model::findOrFail($data)->delete();
    }
}
