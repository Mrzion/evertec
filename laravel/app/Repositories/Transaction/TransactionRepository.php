<?php
namespace App\Repositories\Transaction;

use App\Repositories\Repository;
use App\Models\Transaction;

/**
 * Repository dde Transacciones
 */
class TransactionRepository extends Repository
{
     /**
     * @const RELATIONS
     * Relationship Repository
     */
    const RELATIONS =
    [

    ];


    /**
     * Construct.
     */
    public function __construct(Transaction $model)
    {
        parent::__construct($model, self::RELATIONS);
    }

    public function getTransactionPaginate($pagination=20){
        return $this->model::with(SELF::RELATIONS)->orderBy('created_at','desc')
        ->paginate($pagination);
    }

    public function saveTransaction(object $data){

        return $this->model::create([
            'status'=>$data->response->status->status,
            'reason'=>$data->response->status->reason,
            'message'=>$data->response->status->message,
            'date'=>$data->response->status->date,
            'requestId'=>$data->response->requestId,
            'processUrl'=>$data->response->processUrl,
            'order_id'=>$data->id,
        ]);
    }
}
