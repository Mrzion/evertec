<?php
namespace App\Repositories\Order;

use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Model;
use App\Models\Order;
/**
 * Repository dde Ordenes
 */
class OrderRepository extends Repository
{
     /**
     * @const RELATIONS
     * Relationship Repository
     */
    const RELATIONS =
    [

    ];


    /**
     * Construct.
     */
    public function __construct(Order $model)
    {
        parent::__construct($model, self::RELATIONS);
    }

    public function getOrdersPaginate($pagination=20){
        return $this->model::with(SELF::RELATIONS)->orderBy('created_at','desc')
        ->paginate($pagination);
    }
}
