<?php
namespace App\Services\WebCheckOut;

use App\Models\Order;
use Throwable;
use Illuminate\Http\Request;


/**
 * Servicio para generar la accion de irme contra PlaceToPay y luego dar una respuesta para generar la transaccion y link del usaurio
 */
class GetTransactionService
{

    /**
     * @param Facade\FlareClient\Http\Response $response response.
     *
     */
    public $response;

    /**
     * @param Illuminate\Http\Request $request
     *
     */
    private function __construct(Request $request,Order $order)
    {

        // Actions
        $this->doStore($request,$order);
    }

    /**
     * @param Illuminate\Http\Request $request
     *
     */
    public static function take(Request $request,Order $order): self
    {
        return new static($request,$order);
    }

    /**
     * @param Illuminate\Http\Request $request
     * MEtodo que se encarga de ejecutar el servicio, esta respuesta la da en la variable response
     * @return void
     */
    private function doStore(Request $request,$order): void
    {
        try{
            $request->merge([
                'id'=>$order->id,
                'requestId'=>$order->transaction->requestId
            ]);
           $this->response = $this->proccesing($request);
        }catch(Throwable $e){
            $this->response = $e->getMessage() ;
        }
    }


    //Peticion Curl para conectarme con WebCheckOut
    private function proccesing(Request $request){
        $payload = payload($request,true);
        $curl = curl_init(env('URL_PLACE_TO_PAY').'/'.$request->requestId);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($payload));
        //set the content type to application/json
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //return response instead of outputting
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //execute the POST request
        $result = curl_exec($curl);
        //close cURL resource
        curl_close($curl);
        return $result;

    }



}

