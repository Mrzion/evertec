<?php
namespace App\Services\Transaction;

use Throwable;
use Illuminate\Http\Request;
use App\Repositories\Transaction\TransactionRepository;

/**
 * Servicio para generar una nueva transaccion
 * @author Mario Rangel
 */
class GenerateTransactionService
{
    private $repository;
    /**
     * @param Facade\FlareClient\Http\Response $response response.
     *
     */
    public $response;

    /**
     * @param Illuminate\Http\Request $request
     * @param object $repositories Repositories.
     *
     */
    private function __construct(Request $request,TransactionRepository $repository)
    {
        $this->repository =$repository;
        // Actions
        $this->doStore($request);
    }

    /**
     * @param Illuminate\Http\Request $request
     * @param object $repositories Repositories.
     *
     */
    public static function take(Request $request,TransactionRepository $repository): self
    {
        return new static($request,$repository);
    }

    /**
     * @param Illuminate\Http\Request $request
     *
     * @return void
     */
    private function doStore(Request $request): void
    {
        try{

           $this->response = $this->repository->saveTransaction($request);
        }catch(Throwable $e){
            $this->response = $e->getMessage() ;
        }
    }




}

