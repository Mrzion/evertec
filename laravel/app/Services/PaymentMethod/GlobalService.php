<?php

namespace App\Services\PaymentMethod;

use App\Models\Order;
use App\Services\Transaction\GenerateTransactionService;
use Illuminate\Http\Request;
use App\Strategy\App\Context;
use App\Strategy\PaymentMethod\PlaceToPayStrategy;
use Illuminate\Database\Eloquent\Model;
/**
 * Servicio Global para manipular las estrategias
 * @author Mario Rangel
 */
class GlobalService
{
    private $repositories;
    /**
     * @var Facade\FlareClient\Http\Response $response response.
     *
     */
    public $response;

    /**
     * @var array
     *
     */
    protected $methods = [
        '1' => PlaceToPayStrategy::class,
    ];

    /**
     * RegisterPayment constructor.
     *
     */
    private function __construct(Request $request,Model $model, object $respositories)
    {
        $this->repositories =$respositories;
        $this->run($request,$model, $respositories);
    }

    /**
     * RegisterPayment constructor.
     *
     */
    public static function take(Request $request, Model $model,object $respositories): self
    {
        return new static($request,$model, $respositories);
    }

    /**
     * @param Illuminate\Http\Request $request informacion de la peticion.
     * @param object $repositories repositories.
     * Ejecuto la estrategia con su contexto
     *
     */
    public function run(Request $request,Model $model, object $repositories)
    {
        $order = $model;
        $strategy = (new Context(new $this->methods[$request->type](
            $order,
            $request,
        )))->executeStrategy($order);

        $transaction=$this->executeTransaction($request,$strategy,$order);
        $this->response=(object)[
            'transaction'=>$transaction
        ];

    }

    /**
     * @param $request
     * @param $response
     * @param $order
     * genero el servicio para que cree la transaccion
     */
    private function executeTransaction($request,$response,$order){
            if(isset($response)&&$response->status->status =='OK'){
               $request->merge([
                   'response'=>$response,
                   'id'=>$order->id
               ]);
               return GenerateTransactionService::take($request,$this->repositories->transactionRepository)->response;
           }
       return null;
    }
}
