<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Model;

interface StrategyInterface
{
    public function run(Model $model);
}
