<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class RoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'Admin',
            'email'=>'evertec@admin.com',
            'password'=>Hash::make('evertec@admin.com'),
            'is_admin'=>true
        ]);
        User::create([
            'name'=>'Client',
            'email'=>'evertec@client.com',
            'password'=>Hash::make('evertec@client.com'),
        ]);
    }
}
